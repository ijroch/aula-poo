require_relative 'Person.rb'

class Teacher < Person

    attr_accessor :degree, :subject

    def initialize (social, name, age, birth, address, degree, subject)

        super(social, name, age, birth, address)

        @degree = degree
        @subject = subject
    end
    def apresentationProf()
        puts "Sou formado em "
        degree.each{|item| puts item}
        puts "e dou aula de "
        subject.each{|item| puts item}
    end
end

    