require_relative 'Person.rb'

class Student < Person

    attr_accessor :register, :isStudying

    def initialize social, name, age, birth, address, register

        super(social, name, age,birth, address)
        @register = register

    end

    def isStudying
        @isStudying = true;
    end

    def isMoving
        @isStudying = false
    end

    def apresentationS()
        puts "minha atividade: "
        if(@isStudying)
            puts "estudando"
        else
            puts "locomovendo"
        end
    end       

end